package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load() // 👈 load .env file
	if err != nil {
		log.Fatal(err)
	}
	// return os.Getenv(key)
	// Set the folder path to monitor
	// dotenv := goDotEnvVariable("FOLDER_NIR")
	environment := os.Getenv("FOLDER_NIR")
	// fmt.Println(os.Getenv("FOLDER_NIR"))
	// folderPath := "D:\\tmp\\"
	folderPath := environment

	// Create a new watcher instance
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	// Start a goroutine to handle events
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}

				if event.Op&fsnotify.Create == fsnotify.Create {
					fileName := filepath.Base(event.Name)
					log.Println("New file created:", "\\"+fileName)

					file, err := os.Open(folderPath + "\\" + fileName)
					if err != nil {
						fmt.Println("Error:", err)
						return
					}
					defer file.Close()

					time.Sleep(1 * time.Second)

					reader := csv.NewReader(file)

					records, err := reader.ReadAll()
					if err != nil {
						fmt.Println("Error reading CSV file:", err)
						return
					}

					columnIndex := 25
					var columnValues []string
					for _, record := range records {
						if columnIndex < len(record) {
							columnValues = append(columnValues, "Tanggal : "+record[columnIndex])
							columnValues = append(columnValues, "Jam Analisa : "+record[columnIndex+1])
							columnValues = append(columnValues, "NO Analisa : "+record[columnIndex+2])
							columnValues = append(columnValues, "BRIX : "+record[columnIndex+3])
							columnValues = append(columnValues, "POL : "+record[columnIndex+4])
							columnValues = append(columnValues, "HK : "+record[columnIndex+5])
							//
						}
						fmt.Printf("insert into tablename tgl_analisa, jam_analisa, brix, pol, hk values(%s,%s,%s,%s,%s,%s)\n", record[columnIndex], record[columnIndex+1], record[columnIndex+2], record[columnIndex+3], record[columnIndex+4], record[columnIndex+5])

					}

					// Print the values from the specified column
					for _, value := range columnValues {
						fmt.Println(value)
					}

				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("Error:", err)
			}

		}
	}()

	// Add the folder path to the watcher
	err = watcher.Add(folderPath)
	if err != nil {
		log.Fatal(err)
	}

	// Wait for program termination
	done := make(chan bool)
	<-done
}
